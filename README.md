# economics

economics notes


- Khan-macro [可汗学院网课 宏观经济学 http://open.163.com/special/Khan/macroeconomics.html](http://open.163.com/special/Khan/macroeconomics.html)

- Mankiw-POE-micro 曼昆 经济学原理 微观经济学分册

- Mankiw-POE-macro 曼昆 经济学原理 宏观经济学分册



## Apache License

Copyright 2019 ParrySMS
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.